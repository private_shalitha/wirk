'use strict';

describe('module: main, controller: ManagerChatCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var ManagerChatCtrl;
  beforeEach(inject(function ($controller) {
    ManagerChatCtrl = $controller('ManagerChatCtrl');
  }));

  it('should do something', function () {
    expect(!!ManagerChatCtrl).toBe(true);
  });

});
