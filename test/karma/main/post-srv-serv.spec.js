'use strict';

describe('module: main, service: PostSrv', function () {

  // load the service's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate service
  var PostSrv;
  beforeEach(inject(function (_PostSrv_) {
    PostSrv = _PostSrv_;
  }));

  it('should do something', function () {
    expect(!!PostSrv).toBe(true);
  });

});
