'use strict';

describe('module: main, service: UtilSrv', function () {

  // load the service's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate service
  var UtilSrv;
  beforeEach(inject(function (_UtilSrv_) {
    UtilSrv = _UtilSrv_;
  }));

  it('should do something', function () {
    expect(!!UtilSrv).toBe(true);
  });

});
