'use strict';

describe('module: main, controller: SurveyCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var SurveyCtrl;
  beforeEach(inject(function ($controller) {
    SurveyCtrl = $controller('SurveyCtrl');
  }));

  it('should do something', function () {
    expect(!!SurveyCtrl).toBe(true);
  });

});
