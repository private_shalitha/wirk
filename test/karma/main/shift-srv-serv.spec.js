'use strict';

describe('module: main, service: ShiftSrv', function () {

  // load the service's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate service
  var ShiftSrv;
  beforeEach(inject(function (_ShiftSrv_) {
    ShiftSrv = _ShiftSrv_;
  }));

  it('should do something', function () {
    expect(!!ShiftSrv).toBe(true);
  });

});
