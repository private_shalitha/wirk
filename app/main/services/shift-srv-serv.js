'use strict';
angular.module('main')
.service('ShiftSrv', function ($log, UtilSrv, $q) {
    var self = this;

  // $log.log('Hello from your Service: ShiftSrv in module main');

  self.getNew = function(){
    return $q.when(shifts.slice(0, 3));
  }

  self.getAll = function(){
    console.log('getAll');
    return $q.when(shifts.slice(2, 30));
  }


  var shifts = [{
    "id": 1,
    "category_name": "Services",
    "avatar": "assets/avatars/avatar-finn.png",
    "start_date": "2016-05-03 06:00:02",
    "end_date": "2016-08-27 01:50:09"
  }, {
    "id": 2,
    "category_name": "Training",
    "avatar": "https://robohash.org/eaveniamest.png?size=50x50&set=set1",
    "start_date": "2015-11-18 05:54:45",
    "end_date": "2016-06-10 02:00:37"
  }, {
    "id": 3,
    "category_name": "Engineering",
    "avatar": "https://robohash.org/estquosaccusamus.png?size=50x50&set=set1",
    "start_date": "2016-08-02 01:50:32",
    "end_date": "2016-10-05 15:40:04"
  }, {
    "id": 4,
    "category_name": "Marketing",
    "avatar": "https://robohash.org/consequaturutconsequatur.png?size=50x50&set=set1",
    "start_date": "2016-01-15 18:00:02",
    "end_date": "2016-08-21 07:58:46"
  }, {
    "id": 5,
    "category_name": "Legal",
    "avatar": "https://robohash.org/reprehenderitmolestiaemagni.png?size=50x50&set=set1",
    "start_date": "2016-07-27 23:12:44",
    "end_date": "2016-10-25 00:50:40"
  }, {
    "id": 6,
    "category_name": "Business Development",
    "avatar": "https://robohash.org/velteneturrepudiandae.png?size=50x50&set=set1",
    "start_date": "2016-07-27 09:13:54",
    "end_date": "2016-03-01 04:40:19"
  }, {
    "id": 7,
    "category_name": "Accounting",
    "avatar": "https://robohash.org/etreprehenderitculpa.png?size=50x50&set=set1",
    "start_date": "2016-06-06 21:14:08",
    "end_date": "2016-02-29 05:51:13"
  }, {
    "id": 8,
    "category_name": "Support",
    "avatar": "https://robohash.org/rationeplaceatmaxime.png?size=50x50&set=set1",
    "start_date": "2016-06-19 18:29:58",
    "end_date": "2016-07-24 17:49:06"
  }, {
    "id": 9,
    "category_name": "Research and Development",
    "avatar": "https://robohash.org/quosfacerequia.png?size=50x50&set=set1",
    "start_date": "2016-01-25 12:04:04",
    "end_date": "2016-03-16 19:18:34"
  }, {
    "id": 10,
    "category_name": "Product Management",
    "avatar": "https://robohash.org/voluptasnesciuntmaxime.png?size=50x50&set=set1",
    "start_date": "2015-12-24 11:43:47",
    "end_date": "2016-08-22 16:17:59"
  }, {
    "id": 11,
    "category_name": "Legal",
    "avatar": "https://robohash.org/molestiaequiquidem.png?size=50x50&set=set1",
    "start_date": "2016-02-08 19:18:25",
    "end_date": "2016-10-26 22:14:26"
  }, {
    "id": 12,
    "category_name": "Marketing",
    "avatar": "https://robohash.org/necessitatibusmaioresautem.png?size=50x50&set=set1",
    "start_date": "2016-01-23 12:00:17",
    "end_date": "2016-02-22 03:22:40"
  }, {
    "id": 13,
    "category_name": "Product Management",
    "avatar": "https://robohash.org/veniamerrorodio.png?size=50x50&set=set1",
    "start_date": "2016-06-13 05:35:25",
    "end_date": "2016-08-21 09:55:08"
  }, {
    "id": 14,
    "category_name": "Engineering",
    "avatar": "https://robohash.org/beataesequiquia.png?size=50x50&set=set1",
    "start_date": "2016-08-22 06:33:57",
    "end_date": "2016-01-31 08:01:02"
  }, {
    "id": 15,
    "category_name": "Product Management",
    "avatar": "https://robohash.org/commodidelectusut.png?size=50x50&set=set1",
    "start_date": "2016-03-15 13:09:49",
    "end_date": "2015-11-12 04:47:57"
  }, {
    "id": 16,
    "category_name": "Research and Development",
    "avatar": "https://robohash.org/cupiditateporrofugiat.png?size=50x50&set=set1",
    "start_date": "2015-12-13 21:09:40",
    "end_date": "2016-07-15 18:36:27"
  }, {
    "id": 17,
    "category_name": "Human Resources",
    "avatar": "https://robohash.org/estcommodivoluptas.png?size=50x50&set=set1",
    "start_date": "2016-04-20 05:39:18",
    "end_date": "2016-08-06 12:44:32"
  }, {
    "id": 18,
    "category_name": "Human Resources",
    "avatar": "https://robohash.org/laudantiumestmagni.png?size=50x50&set=set1",
    "start_date": "2016-09-24 16:32:01",
    "end_date": "2016-07-20 09:41:31"
  }, {
    "id": 19,
    "category_name": "Business Development",
    "avatar": "https://robohash.org/omnisnihillaudantium.png?size=50x50&set=set1",
    "start_date": "2016-06-02 15:39:09",
    "end_date": "2016-08-14 20:35:47"
  }, {
    "id": 20,
    "category_name": "Marketing",
    "avatar": "https://robohash.org/consequatureumsint.png?size=50x50&set=set1",
    "start_date": "2016-06-09 15:19:25",
    "end_date": "2016-10-10 21:09:37"
  }, {
    "id": 21,
    "category_name": "Human Resources",
    "avatar": "https://robohash.org/itaquenecessitatibusveritatis.png?size=50x50&set=set1",
    "start_date": "2016-02-28 05:04:58",
    "end_date": "2016-04-13 08:49:39"
  }, {
    "id": 22,
    "category_name": "Sales",
    "avatar": "https://robohash.org/molestiaequianon.png?size=50x50&set=set1",
    "start_date": "2016-01-22 20:00:55",
    "end_date": "2016-01-06 21:02:08"
  }, {
    "id": 23,
    "category_name": "Sales",
    "avatar": "https://robohash.org/autnonvoluptas.png?size=50x50&set=set1",
    "start_date": "2015-12-03 12:20:26",
    "end_date": "2016-10-16 12:15:02"
  }, {
    "id": 24,
    "category_name": "Business Development",
    "avatar": "https://robohash.org/utmagnamut.png?size=50x50&set=set1",
    "start_date": "2016-10-03 17:09:14",
    "end_date": "2016-04-13 15:29:19"
  }, {
    "id": 25,
    "category_name": "Legal",
    "avatar": "https://robohash.org/odiomaioreset.png?size=50x50&set=set1",
    "start_date": "2016-03-25 02:32:46",
    "end_date": "2016-08-22 14:57:55"
  }, {
    "id": 26,
    "category_name": "Legal",
    "avatar": "https://robohash.org/temporasapienteet.png?size=50x50&set=set1",
    "start_date": "2016-10-29 00:32:28",
    "end_date": "2016-07-07 00:19:25"
  }, {
    "id": 27,
    "category_name": "Training",
    "avatar": "https://robohash.org/autquifuga.png?size=50x50&set=set1",
    "start_date": "2016-02-04 03:59:28",
    "end_date": "2016-10-20 00:12:52"
  }, {
    "id": 28,
    "category_name": "Support",
    "avatar": "https://robohash.org/reprehenderitatdolores.png?size=50x50&set=set1",
    "start_date": "2016-03-26 19:38:00",
    "end_date": "2016-10-06 13:24:18"
  }, {
    "id": 29,
    "category_name": "Business Development",
    "avatar": "https://robohash.org/cupiditatetotampraesentium.png?size=50x50&set=set1",
    "start_date": "2016-02-18 04:27:18",
    "end_date": "2016-05-26 15:14:00"
  }, {
    "id": 30,
    "category_name": "Support",
    "avatar": "https://robohash.org/impediteadicta.png?size=50x50&set=set1",
    "start_date": "2015-12-06 19:44:24",
    "end_date": "2016-05-29 22:05:34"
  }, {
    "id": 31,
    "category_name": "Business Development",
    "avatar": "https://robohash.org/voluptatemautemlaborum.png?size=50x50&set=set1",
    "start_date": "2016-03-06 00:52:14",
    "end_date": "2016-09-13 10:58:04"
  }, {
    "id": 32,
    "category_name": "Legal",
    "avatar": "https://robohash.org/consequaturveritatisquia.png?size=50x50&set=set1",
    "start_date": "2016-07-04 22:49:00",
    "end_date": "2016-07-17 07:33:24"
  }, {
    "id": 33,
    "category_name": "Engineering",
    "avatar": "https://robohash.org/utetvoluptas.png?size=50x50&set=set1",
    "start_date": "2015-11-05 00:54:54",
    "end_date": "2016-07-24 08:09:33"
  }, {
    "id": 34,
    "category_name": "Support",
    "avatar": "https://robohash.org/dignissimossuntut.png?size=50x50&set=set1",
    "start_date": "2016-04-18 23:12:50",
    "end_date": "2016-03-04 12:31:48"
  }, {
    "id": 35,
    "category_name": "Sales",
    "avatar": "https://robohash.org/iustoutsit.png?size=50x50&set=set1",
    "start_date": "2015-11-19 18:09:42",
    "end_date": "2016-05-29 12:15:55"
  }, {
    "id": 36,
    "category_name": "Engineering",
    "avatar": "https://robohash.org/aliquidquisquamdolorem.png?size=50x50&set=set1",
    "start_date": "2016-08-05 18:16:14",
    "end_date": "2015-11-13 19:37:34"
  }, {
    "id": 37,
    "category_name": "Legal",
    "avatar": "https://robohash.org/numquamautemullam.png?size=50x50&set=set1",
    "start_date": "2015-12-23 07:11:09",
    "end_date": "2016-03-29 19:35:48"
  }, {
    "id": 38,
    "category_name": "Services",
    "avatar": "https://robohash.org/beataevelitquo.png?size=50x50&set=set1",
    "start_date": "2015-12-01 14:48:12",
    "end_date": "2016-05-28 10:51:27"
  }, {
    "id": 39,
    "category_name": "Legal",
    "avatar": "https://robohash.org/dictaperferendissunt.png?size=50x50&set=set1",
    "start_date": "2016-02-06 21:02:11",
    "end_date": "2016-08-05 03:46:00"
  }, {
    "id": 40,
    "category_name": "Legal",
    "avatar": "https://robohash.org/sintcupiditatedolor.png?size=50x50&set=set1",
    "start_date": "2016-02-22 18:45:42",
    "end_date": "2016-07-30 19:57:12"
  }, {
    "id": 41,
    "category_name": "Product Management",
    "avatar": "https://robohash.org/magnamipsain.png?size=50x50&set=set1",
    "start_date": "2016-05-28 02:29:31",
    "end_date": "2016-10-09 10:42:03"
  }, {
    "id": 42,
    "category_name": "Support",
    "avatar": "https://robohash.org/magnidolorducimus.png?size=50x50&set=set1",
    "start_date": "2016-03-01 04:51:05",
    "end_date": "2016-10-01 04:50:34"
  }, {
    "id": 43,
    "category_name": "Accounting",
    "avatar": "https://robohash.org/autemautconsequatur.png?size=50x50&set=set1",
    "start_date": "2016-09-02 18:19:59",
    "end_date": "2016-07-07 17:43:09"
  }, {
    "id": 44,
    "category_name": "Research and Development",
    "avatar": "https://robohash.org/aperiamdolorecupiditate.png?size=50x50&set=set1",
    "start_date": "2016-05-10 05:01:36",
    "end_date": "2016-09-11 10:46:30"
  }, {
    "id": 45,
    "category_name": "Accounting",
    "avatar": "https://robohash.org/inrerumsint.png?size=50x50&set=set1",
    "start_date": "2016-03-01 02:27:50",
    "end_date": "2015-11-07 10:56:35"
  }, {
    "id": 46,
    "category_name": "Marketing",
    "avatar": "https://robohash.org/nonquodblanditiis.png?size=50x50&set=set1",
    "start_date": "2016-04-17 10:19:22",
    "end_date": "2016-04-22 14:44:12"
  }, {
    "id": 47,
    "category_name": "Marketing",
    "avatar": "https://robohash.org/sapienterepellendusest.png?size=50x50&set=set1",
    "start_date": "2016-01-03 14:09:31",
    "end_date": "2016-09-06 07:38:51"
  }, {
    "id": 48,
    "category_name": "Business Development",
    "avatar": "https://robohash.org/magnimodiaut.png?size=50x50&set=set1",
    "start_date": "2015-11-23 00:51:19",
    "end_date": "2016-01-30 23:03:02"
  }, {
    "id": 49,
    "category_name": "Support",
    "avatar": "https://robohash.org/facilisillomolestiae.png?size=50x50&set=set1",
    "start_date": "2016-02-11 05:27:04",
    "end_date": "2016-10-17 00:21:02"
  }, {
    "id": 50,
    "category_name": "Training",
    "avatar": "https://robohash.org/perferendisiustovoluptate.png?size=50x50&set=set1",
    "start_date": "2015-12-28 21:06:04",
    "end_date": "2016-07-14 03:53:18"
  }, {
    "id": 51,
    "category_name": "Marketing",
    "avatar": "https://robohash.org/sitnostrumquos.png?size=50x50&set=set1",
    "start_date": "2016-09-01 23:25:01",
    "end_date": "2016-07-23 01:21:10"
  }, {
    "id": 52,
    "category_name": "Product Management",
    "avatar": "https://robohash.org/iuresintquos.png?size=50x50&set=set1",
    "start_date": "2016-10-15 04:57:08",
    "end_date": "2016-05-12 22:06:47"
  }, {
    "id": 53,
    "category_name": "Support",
    "avatar": "https://robohash.org/repellendusmodiimpedit.png?size=50x50&set=set1",
    "start_date": "2016-02-06 19:26:57",
    "end_date": "2016-04-25 04:38:46"
  }, {
    "id": 54,
    "category_name": "Training",
    "avatar": "https://robohash.org/temporautsed.png?size=50x50&set=set1",
    "start_date": "2016-04-14 04:34:34",
    "end_date": "2016-08-26 17:30:08"
  }, {
    "id": 55,
    "category_name": "Research and Development",
    "avatar": "https://robohash.org/doloremqueadipisciex.png?size=50x50&set=set1",
    "start_date": "2016-05-09 03:40:06",
    "end_date": "2016-07-03 09:29:36"
  }, {
    "id": 56,
    "category_name": "Business Development",
    "avatar": "https://robohash.org/asperioresnisiharum.png?size=50x50&set=set1",
    "start_date": "2016-09-10 13:40:49",
    "end_date": "2016-05-12 10:24:35"
  }, {
    "id": 57,
    "category_name": "Legal",
    "avatar": "https://robohash.org/adipiscimagniqui.png?size=50x50&set=set1",
    "start_date": "2015-12-29 11:49:23",
    "end_date": "2016-03-08 21:24:07"
  }, {
    "id": 58,
    "category_name": "Human Resources",
    "avatar": "https://robohash.org/suscipitcupiditatereprehenderit.png?size=50x50&set=set1",
    "start_date": "2016-07-29 07:07:04",
    "end_date": "2016-01-27 16:05:54"
  }, {
    "id": 59,
    "category_name": "Accounting",
    "avatar": "https://robohash.org/eiusnonsint.png?size=50x50&set=set1",
    "start_date": "2016-09-11 20:32:32",
    "end_date": "2016-07-12 04:34:31"
  }, {
    "id": 60,
    "category_name": "Services",
    "avatar": "https://robohash.org/estquicommodi.png?size=50x50&set=set1",
    "start_date": "2016-01-21 10:08:38",
    "end_date": "2016-05-20 19:24:32"
  }, {
    "id": 61,
    "category_name": "Training",
    "avatar": "https://robohash.org/odiomolestiaelibero.png?size=50x50&set=set1",
    "start_date": "2016-01-28 20:26:53",
    "end_date": "2016-06-17 23:05:59"
  }, {
    "id": 62,
    "category_name": "Training",
    "avatar": "https://robohash.org/autmolestiaelabore.png?size=50x50&set=set1",
    "start_date": "2016-10-26 19:06:09",
    "end_date": "2015-12-16 06:01:14"
  }, {
    "id": 63,
    "category_name": "Research and Development",
    "avatar": "https://robohash.org/explicaboetomnis.png?size=50x50&set=set1",
    "start_date": "2016-02-23 23:35:21",
    "end_date": "2016-09-01 04:33:41"
  }, {
    "id": 64,
    "category_name": "Training",
    "avatar": "https://robohash.org/quibusdampossimuslaboriosam.png?size=50x50&set=set1",
    "start_date": "2016-07-29 22:18:35",
    "end_date": "2016-04-20 20:27:49"
  }, {
    "id": 65,
    "category_name": "Marketing",
    "avatar": "https://robohash.org/asperioresassumendadolorem.png?size=50x50&set=set1",
    "start_date": "2016-05-10 07:56:42",
    "end_date": "2016-09-11 04:57:51"
  }, {
    "id": 66,
    "category_name": "Product Management",
    "avatar": "https://robohash.org/cumabexercitationem.png?size=50x50&set=set1",
    "start_date": "2015-12-06 22:08:42",
    "end_date": "2016-02-19 07:26:37"
  }, {
    "id": 67,
    "category_name": "Support",
    "avatar": "https://robohash.org/quasioccaecatiaut.png?size=50x50&set=set1",
    "start_date": "2016-04-03 14:25:45",
    "end_date": "2016-01-31 20:38:15"
  }, {
    "id": 68,
    "category_name": "Marketing",
    "avatar": "https://robohash.org/recusandaeaaut.png?size=50x50&set=set1",
    "start_date": "2016-08-04 16:16:58",
    "end_date": "2016-03-04 20:45:31"
  }, {
    "id": 69,
    "category_name": "Services",
    "avatar": "https://robohash.org/providentaccusamusest.png?size=50x50&set=set1",
    "start_date": "2016-07-12 10:57:03",
    "end_date": "2015-11-29 09:12:53"
  }, {
    "id": 70,
    "category_name": "Business Development",
    "avatar": "https://robohash.org/cumquealiquidexpedita.png?size=50x50&set=set1",
    "start_date": "2016-02-09 21:19:25",
    "end_date": "2016-01-12 18:12:25"
  }, {
    "id": 71,
    "category_name": "Services",
    "avatar": "https://robohash.org/fugitdolorumnostrum.png?size=50x50&set=set1",
    "start_date": "2016-10-11 13:40:08",
    "end_date": "2016-05-30 15:57:07"
  }, {
    "id": 72,
    "category_name": "Research and Development",
    "avatar": "https://robohash.org/nihilinciduntvoluptas.png?size=50x50&set=set1",
    "start_date": "2016-07-19 06:54:38",
    "end_date": "2015-11-30 19:46:07"
  }, {
    "id": 73,
    "category_name": "Research and Development",
    "avatar": "https://robohash.org/doloriurequas.png?size=50x50&set=set1",
    "start_date": "2016-02-26 08:18:02",
    "end_date": "2015-11-01 15:28:57"
  }, {
    "id": 74,
    "category_name": "Marketing",
    "avatar": "https://robohash.org/ipsuminaut.png?size=50x50&set=set1",
    "start_date": "2016-07-08 02:42:37",
    "end_date": "2016-08-03 13:33:53"
  }, {
    "id": 75,
    "category_name": "Research and Development",
    "avatar": "https://robohash.org/recusandaevoluptatescorporis.png?size=50x50&set=set1",
    "start_date": "2016-06-24 02:38:32",
    "end_date": "2016-03-16 14:38:20"
  }, {
    "id": 76,
    "category_name": "Research and Development",
    "avatar": "https://robohash.org/molestiaeenimnulla.png?size=50x50&set=set1",
    "start_date": "2016-02-02 22:31:58",
    "end_date": "2015-11-20 00:03:20"
  }, {
    "id": 77,
    "category_name": "Support",
    "avatar": "https://robohash.org/autemipsamcommodi.png?size=50x50&set=set1",
    "start_date": "2016-07-10 13:28:56",
    "end_date": "2016-04-15 11:59:00"
  }, {
    "id": 78,
    "category_name": "Legal",
    "avatar": "https://robohash.org/placeatetoptio.png?size=50x50&set=set1",
    "start_date": "2016-09-08 05:35:20",
    "end_date": "2016-02-16 04:15:45"
  }, {
    "id": 79,
    "category_name": "Marketing",
    "avatar": "https://robohash.org/suntdeseruntasperiores.png?size=50x50&set=set1",
    "start_date": "2016-09-23 22:48:58",
    "end_date": "2016-09-02 07:41:47"
  }, {
    "id": 80,
    "category_name": "Product Management",
    "avatar": "https://robohash.org/etnullaunde.png?size=50x50&set=set1",
    "start_date": "2016-03-23 23:41:14",
    "end_date": "2016-10-28 09:44:17"
  }, {
    "id": 81,
    "category_name": "Legal",
    "avatar": "https://robohash.org/veronumquamvoluptatibus.png?size=50x50&set=set1",
    "start_date": "2015-12-04 18:02:31",
    "end_date": "2016-04-16 20:28:28"
  }, {
    "id": 82,
    "category_name": "Human Resources",
    "avatar": "https://robohash.org/sintconsequaturet.png?size=50x50&set=set1",
    "start_date": "2016-03-18 20:10:46",
    "end_date": "2015-11-01 18:24:14"
  }, {
    "id": 83,
    "category_name": "Accounting",
    "avatar": "https://robohash.org/veroaspernatureum.png?size=50x50&set=set1",
    "start_date": "2016-06-08 01:18:56",
    "end_date": "2016-02-04 23:14:13"
  }, {
    "id": 84,
    "category_name": "Sales",
    "avatar": "https://robohash.org/nequeearumquas.png?size=50x50&set=set1",
    "start_date": "2016-05-02 06:49:33",
    "end_date": "2015-11-08 01:38:15"
  }, {
    "id": 85,
    "category_name": "Support",
    "avatar": "https://robohash.org/aperiamdelectusconsequatur.png?size=50x50&set=set1",
    "start_date": "2016-08-05 20:38:52",
    "end_date": "2016-03-25 10:05:31"
  }, {
    "id": 86,
    "category_name": "Legal",
    "avatar": "https://robohash.org/sitautquod.png?size=50x50&set=set1",
    "start_date": "2016-04-10 09:54:35",
    "end_date": "2016-10-08 11:19:38"
  }, {
    "id": 87,
    "category_name": "Legal",
    "avatar": "https://robohash.org/delenitivoluptatetempore.png?size=50x50&set=set1",
    "start_date": "2016-08-02 01:43:54",
    "end_date": "2016-07-30 14:47:18"
  }, {
    "id": 88,
    "category_name": "Business Development",
    "avatar": "https://robohash.org/sitsolutanon.png?size=50x50&set=set1",
    "start_date": "2016-07-05 09:32:04",
    "end_date": "2016-06-10 14:44:02"
  }, {
    "id": 89,
    "category_name": "Accounting",
    "avatar": "https://robohash.org/odiorerumdoloribus.png?size=50x50&set=set1",
    "start_date": "2016-07-25 01:29:39",
    "end_date": "2016-07-11 18:59:39"
  }, {
    "id": 90,
    "category_name": "Sales",
    "avatar": "https://robohash.org/liberoanimioccaecati.png?size=50x50&set=set1",
    "start_date": "2015-11-02 08:13:59",
    "end_date": "2016-04-06 23:28:14"
  }, {
    "id": 91,
    "category_name": "Services",
    "avatar": "https://robohash.org/dictadelenitisit.png?size=50x50&set=set1",
    "start_date": "2016-09-06 18:37:33",
    "end_date": "2016-10-17 00:03:59"
  }, {
    "id": 92,
    "category_name": "Legal",
    "avatar": "https://robohash.org/occaecatiexpeditaquibusdam.png?size=50x50&set=set1",
    "start_date": "2016-02-10 09:56:15",
    "end_date": "2016-06-25 14:40:42"
  }, {
    "id": 93,
    "category_name": "Legal",
    "avatar": "https://robohash.org/vitaetemporibusrepudiandae.png?size=50x50&set=set1",
    "start_date": "2015-11-07 13:12:12",
    "end_date": "2016-04-20 08:29:26"
  }, {
    "id": 94,
    "category_name": "Services",
    "avatar": "https://robohash.org/quiquiarecusandae.png?size=50x50&set=set1",
    "start_date": "2016-09-20 14:56:44",
    "end_date": "2015-11-06 06:33:06"
  }, {
    "id": 95,
    "category_name": "Training",
    "avatar": "https://robohash.org/quaedoloremut.png?size=50x50&set=set1",
    "start_date": "2016-06-26 23:14:36",
    "end_date": "2016-02-12 10:30:12"
  }, {
    "id": 96,
    "category_name": "Product Management",
    "avatar": "https://robohash.org/namaperiamut.png?size=50x50&set=set1",
    "start_date": "2016-08-09 21:10:25",
    "end_date": "2016-09-15 08:29:18"
  }, {
    "id": 97,
    "category_name": "Product Management",
    "avatar": "https://robohash.org/voluptatumexmagnam.png?size=50x50&set=set1",
    "start_date": "2016-04-17 17:22:02",
    "end_date": "2016-10-26 08:25:35"
  }, {
    "id": 98,
    "category_name": "Accounting",
    "avatar": "https://robohash.org/amolestiaeest.png?size=50x50&set=set1",
    "start_date": "2016-07-16 20:54:49",
    "end_date": "2016-06-15 19:47:46"
  }, {
    "id": 99,
    "category_name": "Services",
    "avatar": "https://robohash.org/maioresipsummagnam.png?size=50x50&set=set1",
    "start_date": "2016-06-27 11:13:22",
    "end_date": "2016-03-05 07:22:18"
  }, {
    "id": 100,
    "category_name": "Product Management",
    "avatar": "https://robohash.org/noninventorepraesentium.png?size=50x50&set=set1",
    "start_date": "2016-10-24 19:27:19",
    "end_date": "2016-01-03 02:54:46"
  }];

  self.processShifts = function(){
    // $log.log('process');
    var shiftArray = [];
    angular.forEach(shifts, function(shift){
      shift.start_date = UtilSrv.toDate(shift.start_date);
      shift.end_date = UtilSrv.toDate(shift.end_date);
      // $log.log(shift);
      shiftArray.push(shift);
    });
    shifts = shiftArray;
  }

  self.processShifts();

});
