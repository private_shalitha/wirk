'use strict';
angular.module('main', [
  'ionic',
  'ngCordova',
  'ui.router',
  'ionic-material',
  'ti-segmented-control',
  // TODO: load other modules selected during generation
])
.config(function ($stateProvider, $urlRouterProvider) {

  // ROUTING with ui.router
  $urlRouterProvider.otherwise('/main/list');
  $stateProvider
    // this state is placed in the <ion-nav-view> in the index.html
    .state('main', {
      url: '/main',
      abstract: true,
      templateUrl: 'main/templates/tabs.html',
      controller: 'AppCtrl as ctrl'
    })
      .state('main.home', {
        url: '/home',
        views: {
          'tab-list': {
            templateUrl: 'main/templates/home.html',
            controller: 'HomeCtrl as ctrl'
          },
          // 'fabContent': {
          //   template: '<button id="fab-activity" class="button button-fab button-fab-top-right expanded button-energized-900 flap"><i class="icon ion-paper-airplane"></i></button>',
          //   controller: function ($timeout) {
          //     $timeout(function () {
          //       document.getElementById('fab-activity').classList.toggle('on');
          //     }, 200);
          //   }
          // }
        }
      })
      .state('main.feed', {
        url: '/feed',
        views: {
          'tab-feed': {
            templateUrl: 'main/templates/feed.html',
            controller: 'FeedCtrl as ctrl'
          }
        }
      })
      .state('main.list', {
        url: '/list',
        views: {
          'tab-list': {
            templateUrl: 'main/templates/list.html',
            // controller: 'SomeCtrl as ctrl'
          }
        }
      })
      .state('main.listDetail', {
        url: '/list/detail',
        views: {
          'tab-list': {
            templateUrl: 'main/templates/list-detail.html',
            // controller: 'SomeCtrl as ctrl'
          }
        }
      })
      .state('main.debug', {
        url: '/debug',
        views: {
          'tab-debug': {
            templateUrl: 'main/templates/debug.html',
            controller: 'DebugCtrl as ctrl'
          }
        }
      });
});
