'use strict';
angular.module('main')
.controller('HomeCtrl', function($scope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk, ShiftSrv, $log) {
  // $scope.$parent.showHeader();
  // $scope.$parent.clearFabs();
  // $scope.isExpanded = true;
  // $scope.$parent.setExpanded(true);
  // $scope.$parent.setHeaderFab('right');
  //
  // $timeout(function() {
  //   ionicMaterialMotion.fadeSlideIn({
  //     selector: '.animate-fade-slide-in .item'
  //   });
  // }, 200);

  // Activate ink for controller
  ionicMaterialInk.displayEffect();

  var self = this;
  $scope.newShifts = [];
  $scope.oldShifts = [];
  // $log.log('Hello from your Controller: HomeCtrl in module main:. This is your controller:', this);

  var refresh = function(){
    ShiftSrv.getNew().then(function(data){
      $log.log(data);
      $scope.newShifts = data;
    });

    ShiftSrv.getAll().then(function(data){
      $log.log(data);
      $scope.oldShifts = data;
    });
  };

  // $scope.$on('$ionicView.enter', function(){
    refresh();
  // });

});
